import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AccueilModule } from './webApp/accueil/accueil.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FilmsModule } from './webApp/webApp/films/films.module';

import { FrameworksModule } from './webApp/frameworks/frameworks.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsReactivesModule } from './webApp/forms-reactives/forms-reactives.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AccueilModule,
    FilmsModule,
    FrameworksModule,
    FormsReactivesModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
