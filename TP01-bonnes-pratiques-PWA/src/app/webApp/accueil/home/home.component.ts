import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validator, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations:[
    trigger('openClose', [
      state('open', style({
        height: '200px',
        opacity: 1,
        backgroundColor: '#bada55',
        color: '#fff'
      })),
      state('closed', style({
        height: '50px',
        opacity: 1,
        backgroundColor: '#fff',
        color: '#bada55'
      })),
      transition(
        'open => closed',[
        animate('0.250s')]
      ),
      transition(
        'closed => open',[
        animate('0.500s')]
      ),
    ])
  ]
})
export class HomeComponent implements OnInit {

  public isOpen: boolean;

  public prenom = new FormControl('', Validators.required);

  public couleur = new FormControl({
    value: '',
    disabled: true,
  });

  constructor() {
    this.isOpen = true;
  }

  ngOnInit(): void {}

  public razPrenom = () => {
    this.prenom.setValue('');
  };

  public fnCouleur(color: string) {
    if (color === 'rouge') {
      this.couleur.setValue('Rouge');
    } else if (color === 'vert') {
      this.couleur.setValue('Vert');
    } else if (color === 'bleu') {
      this.couleur.setValue('Bleu');
    }
  }

  public toggle = () => {
    this.isOpen = !this.isOpen;
    console.clear();
    console.log(this.isOpen);

  }
}
