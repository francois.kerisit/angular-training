import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class PanierService {
  // injection de AngularFireStore
  constructor(private _fireStore: AngularFirestore) {}

  public addPanierToFirebase = (data: any) => {
    return new Promise((resolve, reject) => {
      this._fireStore
        .collection('paniers')
        .add(data)
        .then(
          (res) => {},
          (err) => reject(err)
        );
    });
  };

  // récupérer les commandes de firestore
  public getCommandesFilms = () => {
    return this._fireStore.collection('paniers').snapshotChanges(); // temps réel
  };

  public supprCommandeFilm = (doc: any) => {
    return this._fireStore.collection('paniers').doc(doc).delete();
  }
}
