import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.scss'],
})
export class FormGroupComponent implements OnInit {
  // public monForm: FormGroup = new FormGroup({
  //   prenom: new FormControl('', Validators.required),
  //   nom: new FormControl('', Validators.required),
  //   email: new FormControl('', [Validators.email,
  //   Validators.required]),
  //   adresse: new FormGroup({
  //     rue: new FormControl(''),
  //     ville: new FormControl(''),
  //     codePostal: new FormControl(''),
  //   })
  // });

  public monForm = this._fb.group({
    prenom: [''],
    nom: [''],
    email: [''],
    adresse: this._fb.group({
      rue: [''],
      ville: [''],
      codePostal: [''],
    }),
  });

  constructor(private _fb: FormBuilder) {}

  ngOnInit(): void {}

  get fnControls() {
    console.log('fnControls');
    return this.monForm.controls;
  }

  public onSubmit = () => {
    let valid = this.monForm.valid;
    console.log('valid', valid);

    if (valid) {
      console.log(this.monForm.value);
    }
  };
}
