import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrameworksRoutingModule } from './frameworks-routing.module';
import { LandingFwComponent } from './landing-fw/landing-fw.component';
import { HeaderFwComponent } from './header-fw/header-fw.component';
import { BodyFwComponent } from './body-fw/body-fw.component';
import { FooterFwComponent } from './footer-fw/footer-fw.component';
import { AngularComponent } from './children-nav/angular/angular.component';
import { ReactComponent } from './children-nav/react/react.component';
import { VueComponent } from './children-nav/vue/vue.component';


@NgModule({
  declarations: [
    LandingFwComponent,
    HeaderFwComponent,
    BodyFwComponent,
    FooterFwComponent,
    AngularComponent,
    ReactComponent,
    VueComponent
  ],
  imports: [
    CommonModule,
    FrameworksRoutingModule
  ],
  exports: [
    LandingFwComponent,
    HeaderFwComponent,
    BodyFwComponent,
    FooterFwComponent,
    AngularComponent,
    ReactComponent,
    VueComponent
  ]
})
export class FrameworksModule { }
